const WORD_LIST: &'static str = include_str!("./word_list.txt");
const HOW_MANY: u32 = 10;
const CHARS: [char; 26] = [
	'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
	't', 'u', 'v', 'w', 'x', 'y', 'z',
];

use rand::seq::SliceRandom;
use rand::thread_rng;
use rayon::prelude::*;

fn main() {
	let mut word_list = WORD_LIST.split("\n").collect::<Vec<&str>>();
	println!(
		"This pangraminator was compiled with {} words",
		word_list.len()
	);
	{
		println!("Shuffling...");
		let mut rng = thread_rng();
		word_list.as_mut_slice().shuffle(&mut rng);
		println!("Done shuffling.");
	};
	// {
	// 	println!("Sorting by density of unique characters...");
	// 	word_list.as_mut_slice().par_sort_by(|word_a, word_b| {
	// 		let mut chars_a = vec![];
	// 		word_a.chars().for_each(|ch| {
	// 			if !chars_a.contains(&ch) {
	// 				chars_a.push(ch)
	// 			}
	// 		});
	// 		let mut chars_b = vec![];
	// 		word_b.chars().for_each(|ch| {
	// 			if !chars_b.contains(&ch) {
	// 				chars_b.push(ch)
	// 			}
	// 		});
	// 		match chars_a.len().cmp(&chars_b.len()) {
	// 			std::cmp::Ordering::Equal => word_b.len().cmp(&word_a.len()),
	// 			other => other,
	// 		}
	// 	});
	// };
	let mut used_words: Vec<&'static str> = vec![];
	for pangram_index in 1..=HOW_MANY {
		println!("Working on pangram #{}", pangram_index);
		let mut remaining_chars = CHARS.to_vec();
		let mut words: Vec<&'static str> = vec![];
		while remaining_chars.len() > 0 {
			let next_word = {
				let mut scores = word_list
					.par_iter()
					.filter(|word| !used_words.contains(word))
					.filter(|w| w.len() > 0)
					.map(|word| {
						let score = word
							.chars()
							.filter(|ch| remaining_chars.contains(ch))
							.count();
						let loss = ((word.len()) as f64 / score as f64) as usize;
						(*word, score, loss)
					})
					.filter(|v| v.1 > 0)
					.collect::<Vec<(&str, usize, usize)>>();
				scores.par_sort_by(|a, b| {
					b.2.cmp(&a.2)
					// let a_score: i64 = a.1 as i64 - a.2 as i64;
					// let b_score: i64 = b.1 as i64 - b.2 as i64;
					// a_score.cmp(&b_score)
				});
				scores.last().unwrap().0
			};
			used_words.push(next_word);
			words.push(next_word.trim());
			remaining_chars = remaining_chars
				.iter()
				.filter(|ch| next_word.chars().filter(|ch_| ch_ == *ch).count() == 0)
				.map(|ch| *ch)
				.collect::<Vec<char>>();
		}
		println!("{}", words.join(" "))
	}
}
